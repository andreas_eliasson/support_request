class Request < ActiveRecord::Base

  validates :name, presence: {message: "must be provided"}

  validates :email, presence: {message: "must be provided"}

end


