class RequestsController < ApplicationController

  def index

    @requests = Request.all.order("action DESC")
    

  end

  def new

    @request = Request.new

  end

  def create

    @request = Request.new(params.require(:request).permit(:name, :email, :department, :message, :action))

    #@request.action = "Not Done"


    if @request.action == "1"
      @request.action = "Done"  
    else
      @request.action = "Not Done"
    end

      

    if @request.save
      redirect_to requests_path
    else
      render :new
    end

  end

  def show

    @request = Request.find(params[:id])

  end

  def edit

    @request = Request.find(params[:id])  

  end

  def update

    @request = Request.find(params[:id])
    if @request.update_attributes(params.require(:request).permit(:name, :email, :department, :message, :action))
      if @request.action == "1"
        @request.action = "Done"  
      else
        @request.action = "Not Done"
      end
      @request.save
      redirect_to requests_path
    else
      flash.now[:alert] = "There was a problem updating yor form"
    end

  end

  def destroy

    @request = Request.find(params[:id])
    @request.destroy
    flash[:alert] = "Request was deleted successfully"
    redirect_to requests_path

  end

  def search

    #@query = params[:query]
    @zero_result = "Couldn't find any results"
    #@result_array = []

    @requests = Request.where("name LIKE ? OR email LIKE ? OR message LIKE ? ", params[:query], params[:query], params[:query]).paginate(:page => params[:page], :per_page => 1)

  end

end
